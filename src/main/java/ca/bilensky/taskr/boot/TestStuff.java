package ca.bilensky.taskr.boot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.flowable.cmmn.api.CmmnRepositoryService;
import org.flowable.cmmn.api.CmmnRuntimeService;
import org.flowable.cmmn.api.CmmnTaskService;
import org.flowable.cmmn.api.history.HistoricCaseInstance;
import org.flowable.cmmn.api.repository.CaseDefinition;
import org.flowable.cmmn.api.runtime.CaseInstance;
import org.flowable.cmmn.api.runtime.MilestoneInstance;
import org.flowable.cmmn.api.runtime.PlanItemDefinitionType;
import org.flowable.cmmn.api.runtime.PlanItemInstance;
import org.flowable.cmmn.engine.CmmnEngine;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestStuff {

  @Autowired
  CmmnEngine cmmnEngine;

  @Autowired
  CmmnRuntimeService runtimeService;

  @Autowired
  CmmnTaskService taskService;
  
  @Autowired
  CmmnRepositoryService repositoryService;

  public static final String TEST_1 = "cases/Case_Progression_-_MitchTest.cmmn.xml";
  public static final String TEST_2 = "cases/Case_Progression.cmmn.xml";
  public static final String TEST_3 = "cases/Process_Appplication_-_MitchTest.cmmn.xml";

  private static final Map<String, Map<String, Object>> params = new HashMap<>();
  private static final Map<String,Object> caseProgressionMitchTest = new HashMap<>();
  
  //caseProgressionMitchTest.
  //("employerId", "");
  //caseProgressionMitchTest.put("entityPkId", "1234");


  public List<CaseDefinition> listCaseModels() {
    return repositoryService.createCaseDefinitionQuery().list();
  }

  public void doThings(String definitionKey) {

    // Start process and hand in variables
    CmmnTaskService taskService = cmmnEngine.getCmmnTaskService();

    Map<String, Object> params = new HashMap<>();
    params.put("employerId", "4321");
    params.put("entityPkId", "1234");
    params.put("underClientReview", false);
    params.put("underEmployerReview", false);
    
    CaseInstance caseInstance = runtimeService.createCaseInstanceBuilder()
        .caseDefinitionKey(definitionKey)
        .businessKey("mitch-1234")
        .variables(params)
        .start();

    caseInstance = runtimeService.createCaseInstanceQuery().caseInstanceId(caseInstance.getId()).singleResult();


    Scanner scanner = new Scanner(System.in);
    String ans = "";

    while (!ans.equals("y")) {
      logThings(caseInstance.getId(), runtimeService, taskService);

      List<Task> tasks = getTasks();
      List<PlanItemInstance> items = getActionableItems();

      System.out.println("Choose Item to complete (1 to " + tasks.size() + ") or 'y' to exit");
      ans = scanner.nextLine();

      if (!ans.equals("y")) {
        try {
          int index = Integer.parseInt(ans) - 1;
          if (index >= tasks.size()) {
            throw new NumberFormatException();
          }

          
          Task task = tasks.get(index);

          taskService.complete(task.getId());

        } catch (NumberFormatException e) {
          System.out.println("Invalid Response.");
        }
      }
    }

    System.out.println("Goodbye!");
    scanner.close();
  }

  private void logThings(String caseInstanceId, CmmnRuntimeService runtimeService, CmmnTaskService taskService) {
    /* Print list of tasks */
    HistoricCaseInstance instanace = cmmnEngine.getCmmnHistoryService().createHistoricCaseInstanceQuery().caseInstanceId(caseInstanceId).singleResult();

    System.out.println("Case Instance - ID:\t" + instanace.getId());
    System.out.println("Case Instance - Name:\t" + instanace.getName());
    System.out.println("Case Instance - State:\t" + instanace.getState());
    System.out.println();

    List<MilestoneInstance> milestones = runtimeService.createMilestoneInstanceQuery().list();
    System.out.println("You Have " + milestones.size() + " Milestones Items:");
    for (int i = 0; i < milestones.size(); i++) {
      System.out.println((i + 1) + ") " + milestones.get(i).getName() + " : " + milestones.get(i).getId());
    }
    System.out.println();

    List<PlanItemInstance> planItems = runtimeService.createPlanItemInstanceQuery().list();
    System.out.println("You Have " + planItems.size() + " Plan Items:");
    for (int i = 0; i < planItems.size(); i++) {
      System.out.println((i + 1) + ") " + planItems.get(i).getName() + " : " + planItems.get(i).getState() + " : " + planItems.get(i).getStageInstanceId());
    }
    System.out.println();

    List<PlanItemInstance> stages = runtimeService.createPlanItemInstanceQuery()
        .caseInstanceId(caseInstanceId)
        .planItemDefinitionType(PlanItemDefinitionType.STAGE)
        .planItemInstanceStateActive().list();
    System.out.println("You Have " + stages.size() + " Active Stages:");
    for (int i = 0; i < stages.size(); i++) {
      System.out.println((i + 1) + ") " + stages.get(i).getName() + " : " + stages.get(i).getState() + " : " + stages.get(i).getId());

    }
    System.out.println();

    
    
    List<PlanItemInstance> actionItems = getActionableItems();
    System.out.println("You Have " + actionItems.size() + " Enabled Plan Items:");
    for (int i = 0; i < actionItems.size(); i++) {
      System.out.println((i + 1) + ") " + actionItems.get(i).getName() + " : " + actionItems.get(i).getState() + " : " + actionItems.get(i).getPlanItemDefinitionType());
    }
    System.out.println();

    Map<String, Object> variables = runtimeService.createCaseInstanceQuery()
    		.caseInstanceId(caseInstanceId)
    		.includeCaseVariables()
    		.singleResult().getCaseVariables();
    System.out.println("You Have " + variables.size() + " Variables Set:");
    for (String variableKey: variables.keySet()) {
      System.out.println(variableKey + " : " + String.valueOf(variables.get(variableKey)));

    }
    System.out.println();

    
    List<Task> tasks = getTasks();
    System.out.println("You Have " + tasks.size() + " Enabled Actions:");
    for (int i = 0; i < tasks.size(); i++) {
      System.out.println((i + 1) + ") " + tasks.get(i).getName());
    }
    System.out.println();

  }

  private List<PlanItemInstance> getActionableItems() {
	  
    List<PlanItemInstance> tasks = runtimeService.createPlanItemInstanceQuery().planItemInstanceStateActive().list();
    tasks.addAll(runtimeService.createPlanItemInstanceQuery().planItemInstanceStateAvailable().planItemDefinitionType(PlanItemDefinitionType.USER_EVENT_LISTENER).list());
    return tasks;
  }
  
  private List<Task> getTasks() {
  
    List<Task> tasks = taskService.createTaskQuery().active().list();
    return tasks;
  }
}
