package ca.bilensky.taskr.boot;

import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance;
import org.flowable.cmmn.api.delegate.PlanItemJavaDelegate;
import org.springframework.stereotype.Component;

@Component
public class UpdateStatusDelegate implements PlanItemJavaDelegate {

  private String status;

  @Override
  public void execute(DelegatePlanItemInstance planItemInstance) {
    System.out.println("UpdateStatusDelegate - START");
    System.out.println("UpdateStatusDelegate - ID:\t" + planItemInstance.getId());
    System.out.println("UpdateStatusDelegate - Name:\t" + planItemInstance.getName());
    System.out.println("UpdateStatusDelegate - State:\t" + planItemInstance.getState());
    System.out.println("UpdateStatusDelegate - Status:\t" + status);
    System.out.println("UpdateStatusDelegate - END");
  }
}
