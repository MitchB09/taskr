package ca.bilensky.taskr.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("ca.bilensky.taskr")
public class TaskrApplication {
  public static void main(String[] args) {
    SpringApplication.run(TaskrApplication.class, args);
  }
}