package ca.bilensky.taskr.controllers;

import ca.bilensky.taskr.boot.TestStuff;
import org.flowable.cmmn.api.repository.CaseDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    TestStuff testStuff;

    @GetMapping("/")
    List<CaseDefinition> home() {
      return testStuff.listCaseModels();
    }

    @GetMapping("/{processName}")
    public void process2(@PathVariable String processName) {
      testStuff.doThings(processName);
    }



}
